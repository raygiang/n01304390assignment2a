﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Assignment2a._Default" %>

<asp:Content ID="HomepageTitle" ContentPlaceHolderID="TrickyConceptArea" runat="server">
<!-- This content will be placed into the tricky concept content placeholder on my master page -->
    <div>
        <h1>Welcome to my Review Website</h1>
    </div>
</asp:Content>

<asp:Content ID="HomepageSummary" ContentPlaceHolderID="ExampleCodeArea" runat="server">
<!-- This content will be placed into the example code content placeholder on my master page -->
    <div>
        <p>The purpose of this webpage is to act as review for three of my courses this semester.</p>
    </div>
</asp:Content>

<asp:Content ID="HomepageCourseList" ContentPlaceHolderID="OwnCodeArea" runat="server">
<!-- This content will be placed into the content placeholder on my master page for my own code -->
    <div>
        <h2>Courses Covered:</h2>
        <ul>
            <li>HTTP5101 - Web Application Development</li>
            <li>HTTP5103 - Web Programming</li>
            <li>HTTP5105 - Database Design and Development</li>
        </ul>

    </div>
</asp:Content>

<asp:Content ID="HomepageThankYou" ContentPlaceHolderID="HelpfulLinksArea" runat="server">
<!-- This content will be placed into the content placeholder on my master page for helpful links -->
    <div>
        <p>Thank you for visiting my website.</p>
    </div>
</asp:Content>