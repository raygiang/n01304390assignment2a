﻿<%@ Page Title="Web Application Development" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebApplication.aspx.cs" Inherits="Assignment2a.WebApplication" %>

<asp:Content ID="WebApplicationTrickyConcept" ContentPlaceHolderID="TrickyConceptArea" runat="server">
<!-- This content will be placed into the tricky concept content placeholder on my master page -->
    <div>
        <h2>A Tricky Concept</h2>

        <p>
            In this class I found that working in the code behind to retrieve the value
            of different elements in a form to be quite difficult. The reason is because,
            a form can have many elements such as text boxes, drop down lists, check boxes, 
            radio buttons, and more. The syntax for retriving the values for these elements in the 
            code behind can be hard to remember.
        </p>
    </div>
</asp:Content>

<asp:Content ID="WebApplicationExampleCode" ContentPlaceHolderID="ExampleCodeArea" runat="server">
<!-- This content will be placed into the example code content placeholder on my master page -->
    <div>
        <h2>Example Code</h2>
        
        <pre><code>switch (myDogBreed)
{
    case 1:
        Chihuahua mychihuahua = new Chihuahua(myDogName, myDogAge);
        dogRes.InnerHtml = mychihuahua.ChihuahuaTrick(command);
        break;
    case 2:
        Bulldog mybulldog = new Bulldog(myDogName, myDogAge);
        dogRes.InnerHtml += mybulldog.BulldogTrick(command);
        break;
    case 3:
        Shepherd myshepherd = new Shepherd(myDogName, myDogAge);
        dogRes.InnerHtml = myshepherd.ShepherdTrick(command);
        break;
    case 4:
        Rotweiler myrotweiler = new Rotweiler(myDogName, myDogAge);
        dogRes.InnerHtml = myrotweiler.RotweilerTrick(command);
        break;
}</code></pre>

        <p>
            The above is example code from the class about Inheritance. The variable myDogBreed
            will contain a number from 1 to 4 corresponding to a breed of a dog. Depending on
            the breed of the dog, either a Chihuahua, Bulldog, Shepherd, or Rotweiler object
            will be created. All of these objects inherit from their parent class Dog. Their
            respective trick method is then called which in turn calls the Trick method from
            their parent class.
        </p>
    </div>
</asp:Content>

<asp:Content ID="WebApplicationOwnCode" ContentPlaceHolderID="OwnCodeArea" runat="server">
<!-- This content will be placed into the content placeholder on my master page for my own code -->
    <div>
        <h2>My Own Code</h2>

        <pre><code>public double CalculatePrice()
{
    double price = 10.00;

    price += 5.00 * appointment.ServicesRequired.Count;
    if(appointment.GetDog)
    {
        price += 2.50;
    }
    if(appointment.ReturnDog)
    {
        price += 2.50;
    }

    price += 1.00 * (appointment.ReturnDate.Hour - appointment.ReceiveDate.Hour);

    return price;
}</code></pre>

        <p>
            The above is code that I wrote for my second assignment. It is a method of my Booking class
            that uses properties of an appointment object to calculate and return a price.
        </p>
    </div>
</asp:Content>

<asp:Content ID="WebApplicationHelpfulLinks" ContentPlaceHolderID="HelpfulLinksArea" runat="server">
<!-- This content will be placed into the content placeholder on my master page for helpful links -->
    <div>
        <h2>Some Helpful Links</h2>
        <p>Below are a few links that I found to be helpful for this course so far.</p>
        <ul>
            <li><a href="https://www.codeproject.com/Articles/3882/ASP-NET-Validators-Unclouded">ASP.NET Validators - Code Project</a></li>
            <li><a href="https://docs.microsoft.com/en-us/dotnet/api/system.web.ui.webcontrols.checkboxlist?redirectedfrom=MSDN&view=netframework-4.7.2">CheckBoxList Class - Microsoft</a></li>
            <li><a href="https://www.javatpoint.com/asp-net-radiobutton">Radio Buttons - Javatpoint</a></li>
        </ul>
    </div>
</asp:Content>