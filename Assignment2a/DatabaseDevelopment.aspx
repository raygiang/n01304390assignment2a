﻿<%@ Page Title="Database Design and Development" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DatabaseDevelopment.aspx.cs" Inherits="Assignment2a.DatabaseDevelopment" %>

<asp:Content ID="DatabaseDevelopmentTrickyConcept" ContentPlaceHolderID="TrickyConceptArea" runat="server">
<!-- This content will be placed into the tricky concept content placeholder on my master page -->
    <div>
        <h2>A Tricky Concept</h2>

        <p>
            I found the concept of full join to be rather tricky. A full join is similar to an inner join 
            in the sense that it joins two tables by a condition specified in the <code>ON</code> clause. The
            difference is that all the entries that do not satisfy the condition are still shown in the
            resulting table. However, these entries will have null values for all the columns of the opposite
            table.
        </p>
    </div>
</asp:Content>

<asp:Content ID="DatabaseDevelopmentExampleCode" ContentPlaceHolderID="ExampleCodeArea" runat="server">
<!-- This content will be placed into the example code content placeholder on my master page -->
    <div>
        <h2>Example Code</h2>
        
        <pre><code>SELECT primary_class, ROUND(AVG(intelligence), 3) as smarts
FROM characters
GROUP BY primary_class
ORDER BY smarts DESC;</code></pre>

        <p>
            This code is from the class notes on aggregate functions. It returns a table with two
            columns that basically represent a list of all primary_class in the characters table,
            and the average of the intelligence stat for all the characters that have the corresponding
            primary_class. The average intelligence is rounded to three decimal places, and the results 
            are sorted in descending order so the primary classes with the highest average intelligence
            will display first.
        </p>
    </div>
</asp:Content>

<asp:Content ID="DatabaseDevelopmentOwnCode" ContentPlaceHolderID="OwnCodeArea" runat="server">
<!-- This content will be placed into the content placeholder on my master page for my own code -->
    <div>
        <h2>My Own Code</h2>

        <pre><code>SELECT premisetype
FROM autotheft
GROUP BY premisetype
HAVING COUNT(*) > 500;</code></pre>

        <p>
            The above is code that I wrote for an in class lab. It uses an autotheft table that contains 
            reports of vehicle thefts. This statement will produce a result consisting of a table
            with one column that esentially shows a list of premisetypes where there have been over 500 thefts.
        </p>
    </div>
</asp:Content>

<asp:Content ID="DatabaseDevelopmentHelpfulLinks" ContentPlaceHolderID="HelpfulLinksArea" runat="server">
<!-- This content will be placed into the content placeholder on my master page for helpful links -->
    <div>
        <h2>Some Helpful Links</h2>
        <p>Below are a few links that I found to be helpful for this course so far.</p>
        <ul>
            <li><a href="https://simonborer.github.io/db-course/notes/week-4/cheatsheet.html">HTTP5105 Cheat Sheet</a></li>
            <li><a href="https://docs.oracle.com/cd/B19306_01/server.102/b14225/ch4datetime.htm">Datetime Datatypes - Oracle</a></li>
            <li><a href="https://www.w3schools.com/sql/sql_join_full.asp">Full Joins - W3 Schools</a></li>
        </ul>
    </div>
</asp:Content>
