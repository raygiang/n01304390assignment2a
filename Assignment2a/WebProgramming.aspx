﻿<%@ Page Title="Web Programming" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebProgramming.aspx.cs" Inherits="Assignment2a.WebProgramming" %>

<asp:Content ID="WebProgrammingTrickyConcept" ContentPlaceHolderID="TrickyConceptArea" runat="server">
<!-- This content will be placed into the tricky concept content placeholder on my master page -->
    <div>
        <h2>A Tricky Concept</h2>

        <p>
            I personally found the concept of creating objects in javascript to be especially tricky. I am used to 
            having object classes with properties, constructors, and methods. These classes can then be used to create
            instances of the object. In javascript however, you esentially have to create the object class and
            instantiate the object at the same time, which I found to be especially confusing.
        </p>
    </div>
</asp:Content>

<asp:Content ID="WebProgrammingExampleCode" ContentPlaceHolderID="ExampleCodeArea" runat="server">
<!-- This content will be placed into the example code content placeholder on my master page -->
    <div>
        <h2>Example Code</h2>

        <pre><code>var userInput = prompt("Do you prefer red, green, or blue?", "Please say blue.")

if(userInput === "" || userInput === null || userInput === "Please say blue."){
	console.log("You need to enter a colour!");
}
else{
	switch(userInput.toLowerCase().trim()){
		case "red":
			console.log("Red is rad");
			break;
		case "green":
			console.log("Green is great!");
			break;
		case "blue":
			console.log("Blue is best!");
			break;
		default:
			console.log("Not one of the big 3");
			break;
	}
}</code></pre>

        <p>
            The above is example code taken from class. The code asks the user to enter their
            favourite colour. The user input is validated against an empty string, the cancel button,
            and the placeholder text. Then a switch case statement is used to check if the input is either
            red, green, blue, or something else. The appropriate message will be output to the console
            depending on the input.
        </p>
    </div>
</asp:Content>

<asp:Content ID="WebProgrammingOwnCode" ContentPlaceHolderID="OwnCodeArea" runat="server">
<!-- This content will be placed into the content placeholder on my master page for my own code -->
    <div>
        <h2>My Own Code</h2>

        <pre><code>var userChoice;
var enterNumError = "Please enter a number between 1 and 10!";
var validNumFlag = false;

while(validNumFlag === false)
{
	userChoice = parseInt(prompt("Which top 10 book would you like?", "Pick a number: 1-10"));
	if(isNaN(userChoice) || userChoice < 1 || userChoice > 10){
		alert(enterNumError);
	}
	else{
		validNumFlag = true;
	}
}
</code></pre>

        <p>
            The above is an example of code I have written for an assignment in this class. 
            This code will constantly prompt the user asking for a number from 1 to 10 until
            they enter a valid response. The code validates that a number input is entered and
            that the number is greater than 1 and lower than 10.
        </p>
    </div>
</asp:Content>

<asp:Content ID="WebProgrammingHelpfulLinks" ContentPlaceHolderID="HelpfulLinksArea" runat="server">
<!-- This content will be placed into the content placeholder on my master page for helpful links -->
    <div>
        <h2>Some Helpful Links</h2>
        <p>Below are a few links that I found to be helpful for this course so far.</p>
        <ul>
            <li><a href="https://www.w3schools.com/js/js_arrays.asp">Arrays - W3 Schools</a></li>
            <li><a href="https://www.w3schools.com/js/js_objects.asp">Objects - W3 Schools</a></li>
            <li><a href="https://www.w3schools.com/js/js_functions.asp">Functions - W3 Schools</a></li>
        </ul>
    </div>
</asp:Content>